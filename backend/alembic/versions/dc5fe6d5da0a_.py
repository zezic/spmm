"""empty message

Revision ID: dc5fe6d5da0a
Revises: a909c416cd36
Create Date: 2018-11-11 20:54:03.233483

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'dc5fe6d5da0a'
down_revision = 'a909c416cd36'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('club',
    sa.Column('id', postgresql.UUID(), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('name', sa.String(length=256), nullable=True),
    sa.Column('vk_link', sa.String(length=256), nullable=True),
    sa.Column('leader', sa.String(length=256), nullable=True),
    sa.Column('is_displayed', sa.Boolean(), nullable=True),
    sa.Column('body', sa.Text(), nullable=True),
    sa.Column('slug', sa.String(length=256), nullable=True),
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('picture_id', postgresql.UUID(), nullable=True),
    sa.ForeignKeyConstraint(['picture_id'], ['file.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('club_to_picture',
    sa.Column('club_id', postgresql.UUID(), nullable=True),
    sa.Column('file_id', postgresql.UUID(), nullable=True),
    sa.ForeignKeyConstraint(['club_id'], ['club.id'], ),
    sa.ForeignKeyConstraint(['file_id'], ['file.id'], )
    )
    op.create_table('event',
    sa.Column('id', postgresql.UUID(), server_default=sa.text('gen_random_uuid()'), nullable=False),
    sa.Column('name', sa.String(length=256), nullable=True),
    sa.Column('datetime', sa.DateTime(), nullable=True),
    sa.Column('description', sa.String(length=10000), nullable=True),
    sa.Column('slug', sa.String(length=256), nullable=True),
    sa.Column('is_displayed', sa.Boolean(), nullable=True),
    sa.Column('place', sa.String(length=256), nullable=True),
    sa.Column('picture_id', postgresql.UUID(), nullable=True),
    sa.Column('club_id', postgresql.UUID(), nullable=True),
    sa.ForeignKeyConstraint(['club_id'], ['club.id'], ),
    sa.ForeignKeyConstraint(['picture_id'], ['file.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('event')
    op.drop_table('club_to_picture')
    op.drop_table('club')
    # ### end Alembic commands ###
