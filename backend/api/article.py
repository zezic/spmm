"""Article API."""
from .base import BaseAPI, BaseListAPI
from models import Article
from schemas import ArticleSchema


class ArticleAPI(BaseAPI):
    model = Article
    schema = ArticleSchema
    id_field = 'slug'


class ArticleListAPI(BaseListAPI):
    model = Article
    schema = ArticleSchema
