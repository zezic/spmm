"""Base CRUD API endpoint class."""
from sanic.views import HTTPMethodView
from sanic.response import json
from sanic.request import Request

from database import scoped_session
from lib.access import authorized
from lib.querying import instance_by_uid, get_free_slug


class BaseAPI(HTTPMethodView):
    """Base instance API."""
    decorators = [authorized()]

    model = None
    schema = None
    id_field = 'id'

    async def get(self, request: Request, uid):
        """Retrieve model instance."""
        with scoped_session() as session:
            instance = instance_by_uid(session, self.model, self.id_field, uid)
            if instance is None:
                return json({}, status=404)
            schema = self.schema()
            return json(schema.dump(instance).data)

    async def patch(self, request: Request, uid):
        """Modify model instance."""
        data = request.json or {}
        with scoped_session() as session:
            instance = instance_by_uid(session, self.model, self.id_field, uid)
            schema = self.schema(session=session)
            result = schema.load(
                data, instance=instance
            )
            session.commit()
            return json(schema.dump(result.data).data)

    async def delete(self, request: Request, uid):
        """Remove model instance."""
        with scoped_session() as session:
            instance = instance_by_uid(session, self.model, self.id_field, uid)
            session.delete(instance)
            session.commit()
            return json({"message": 'OK'})


def make_arg_getter(sanic_request_args):
    """Creates a helper to unpack request args in a shorter way."""

    def getter(arg_name):
        if arg_name in sanic_request_args:
            arg_value = sanic_request_args.get(arg_name)
            return arg_value[0] if isinstance(arg_value, list) else arg_value
        return None

    return getter


class BaseListAPI(HTTPMethodView):
    """Base instance list API."""
    decorators = [authorized()]

    model = None
    schema = None

    async def get(self, request: Request):
        """Retrieve list of model instances."""
        arg = make_arg_getter(request.args)
        print(request.args)
        page = int(arg('page') or 1)
        page_size = int(arg('page_size') or 6)
        sort_by = arg('sort_by')
        sort_dir = arg('sort_dir')
        schema = self.schema()
        if arg('is_displayed') == 'all':
            request.args.pop('is_displayed')
        else:
            request.args.update({'is_displayed': True})
            
        with scoped_session() as session:
            query = session.query(self.model)

            for field_name, field_value in request.query_args:
                field = getattr(self.model, field_name, None)
                if field:
                    query = query.filter(field == field_value)

            total = query.count()

            if sort_by and hasattr(self.model, sort_by):
                attr = getattr(self.model, sort_by)
                if sort_dir == 'desc':
                    attr = attr.desc()
                query = query.order_by(attr)
                query = query.order_by(self.model.name)

            items = query.limit(page_size).offset((page - 1) * page_size).all()

            return json({
                'items': schema.dump(items, many=True).data,
                'meta': {
                    'total': total
                }
            })

    async def post(self, request: Request):
        """Create new instance of model from incoming data."""
        data = request.json
        with scoped_session() as session:
            schema = self.schema()
            if not data.get('name'):
                return json(
                       {'message': 'Not name'},
                       status=400
                )
            if hasattr(self.model, 'slug'):
                if data.get('type') == 'page':
                    isLowerCase = True
                else:
                    isLowerCase = False
                slug = get_free_slug(session, self.model, data.get('name'), isLowerCase)
                data.update({'slug': slug})
            instance = schema.load(data, session=session).data
            session.add(instance)
            session.commit()
            return json(schema.dump(instance).data)