"""Session API."""
from uuid import uuid4

from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError
from sanic.views import HTTPMethodView
from sanic.response import json

from lib.access import authorized

from database import scoped_session
from models import User


class SessionAPI(HTTPMethodView):
    """Session API."""
    decorators = [authorized(methods=['GET'])]

    async def post(self, request):
        """Request new token."""
        data = request.json or {}
        login = data.get('login')
        password = data.get('password')
        with scoped_session() as session:
            user = session.query(User).filter_by(login=login).first()
            if not user:
                return json({}, status=401)
            ph = PasswordHasher()
            try:
                ph.verify(user.password, password)
            except VerifyMismatchError as e:
                return json({}, status=401)
            redis = request.app.redis
            token = str(uuid4())
            token_string = 'TOKEN:{}'.format(token)
            await redis.setex(token_string, 60 * 60, str(user.id))
            return json({'token': token})

    async def get(self, request):
        """Check token is alive."""
        return json({})
