"""API blueprint with all endpoints."""
from sanic import Blueprint

from .article import ArticleAPI, ArticleListAPI
from .club import ClubAPI, ClubListAPI
from .file import FileAPI
from .session import SessionAPI

api = Blueprint('api_blueprint')

api.add_route(ArticleListAPI.as_view(), 'article')
api.add_route(ArticleAPI.as_view(), 'article/<uid>')

api.add_route(ArticleListAPI.as_view(), 'club')
api.add_route(ArticleAPI.as_view(), 'club/<uid>')

api.add_route(FileAPI.as_view(), 'file')
api.add_route(SessionAPI.as_view(), 'session')
