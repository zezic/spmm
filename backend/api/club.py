"""Club API."""
from .base import BaseAPI, BaseListAPI
from models import Club
from schemas import ClubSchema


class ClubAPI(BaseAPI):
    model = Club
    schema = ClubSchema


class ClubListAPI(BaseListAPI):
    model = Club
    schema = ClubSchema
