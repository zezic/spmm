"""File API."""
import os
from hashlib import md5
from sanic.views import HTTPMethodView
from sanic.response import json

from lib.access import authorized
from lib.paths import path_from_hash_and_ext

from database import scoped_session
from schemas import FileSchema


class FileAPI(HTTPMethodView):
    """File API that allow uploads."""
    decorators = [authorized()]

    async def post(self, request):
        """Upload new file."""
        in_file = request.files.get('file')
        file_name = in_file.name
        file_ext = file_name.split('.')[-1]
        file_bytes = in_file.body
        file_hash = md5(file_bytes).hexdigest()

        file_path = path_from_hash_and_ext(file_hash, file_ext)
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with open(file_path, 'wb') as fd:
            fd.write(file_bytes)

        fields = {
            'name': ''.join(file_name.split('.')[0:-1]),
            'ext': file_ext,
            'size': len(file_bytes),
            'md5': file_hash
        }
        schema = FileSchema()
        with scoped_session() as session:
            instance = schema.load(fields, session=session).data
            session.add(instance)
            session.commit()

            return json(schema.dump(instance).data)
