from getpass import getpass
from argon2 import PasswordHasher
from database import Session
from models import User

print('Enter new user login:')
login = input().strip()

s = Session()

user = s.query(User).filter_by(login=login).first()

if user:
    print('User "{}" is already exists in DB.'.format(user.login))
    print('Enter new password for user "{}".'.format(user.login))
else:
    print('New user "{}" will be created.'.format(login))
    print('Please set password for it.')
    user = User(login=login)

ph = PasswordHasher()
password = getpass()

user.password = ph.hash(password)
s.add(user)
s.commit()
print('Password for user "{}" updated successfully.'.format(user.login))
s.close()
