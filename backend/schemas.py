"""Marshmallow schemas for SQLAlchemy models."""
from marshmallow import fields, pre_load
from marshmallow_sqlalchemy import ModelSchema
from models import Article, File, Event, Club


class ArticleSchema(ModelSchema):
    """Article schema."""

    class Meta:
        """Schema metadata."""

        model = Article

    id = fields.String()

    picture_real = fields.Nested('FileSchema',
                                 attribute='picture',
                                 only=['id', 'url'],
                                 dump_only=True)
    pictures_real = fields.Nested('FileSchema',
                                  attribute='pictures',
                                  many=True,
                                  only=['id', 'url'],
                                  dump_only=True)
    created_at = fields.DateTime(allow_none=True)

    @pre_load
    def set_field_session(self, data):
        """Pass session to child schemas."""
        self.fields['picture_real'].schema.session = self.session
        self.fields['pictures_real'].schema.session = self.session


class FileSchema(ModelSchema):
    """File schema."""

    class Meta:
        """Schema metadata."""

        model = File

    fullname = fields.String(dump_only=True)
    url = fields.String(dump_only=True)
    articles = fields.Nested('ArticleSchema',
                             attribute='articles',
                             only=['id', 'slug'],
                             dump_only=True)


class EventSchema(ModelSchema):
    """Event schema."""

    class Meta:
        """Schema metadata."""

        model = Event

    id = fields.String()

    picture_real = fields.Nested('FileSchema',
                                 attribute='picture',
                                 only=['id', 'url'],
                                 dump_only=True)
    datetime = fields.DateTime()

    @pre_load
    def set_field_session(self, data):
        """Pass session to child schemas."""
        self.fields['picture_real'].schema.session = self.session


class ClubSchema(ModelSchema):
    """Club schema."""

    class Meta:
        """Schema metadata."""

        model = Club

    id = fields.String()

    picture_real = fields.Nested('FileSchema',
                                 attribute='picture',
                                 only=['id', 'url'],
                                 dump_only=True)
    pictures_real = fields.Nested('FileSchema',
                                  attribute='pictures',
                                  many=True,
                                  only=['id', 'url'],
                                  dump_only=True)
    created_at = fields.DateTime()

    @pre_load
    def set_field_session(self, data):
        """Pass session to child schemas."""
        self.fields['picture_real'].schema.session = self.session
        self.fields['pictures_real'].schema.session = self.session
