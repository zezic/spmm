# pylama:ignore=E722
""" This module exports the database engine.
Notes:
     Using the scoped_session contextmanager is
     best practice to ensure the session gets closed
     and reduces noise in code by not having to manually
     commit or rollback the db if a exception occurs.
"""
# import os
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from instance import config as ic

# engine = create_engine(os.environ['DATABASE_URL'])
engine = create_engine(
    'postgresql://{user}:{password}@localhost:{port}/{database}'.format(
        user=ic.DB_USER,
        password=ic.DB_PASSWORD,
        database=ic.DB_DATABASE,
        port=ic.DB_PORT
    )
)

# Session to be used throughout app.
Session = sessionmaker(bind=engine)


@contextmanager
def scoped_session():
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()
