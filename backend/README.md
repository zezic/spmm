# SPMM website backend

## Deploying

### Install system dependencies

    sudo apt update
    sudo apt install postgresql postgresql-contrib libpq-dev python3-venv redis-server

### Create UNIX user, PostgreSQL user and database

    sudo -u postgres createuser spmm
    sudo -u postgres psql -c "alter user spmm with password 'spmm_password';"

    sudo -u postgres psql -d template1 -c 'create extension pgcrypto;'

    sudo -u postgres createdb -O spmm spmm

### Configure PostgreSQL

If you want to access the DB without having corresponding UNIX user created, then edit the configuration file located at `/etc/postgresql/<version>/main/pg_hba.conf` and change the `peer` authentication method to `md5`:

```
# "local" is for Unix domain socket connections only
local   all             all                                     md5
```

### Prepare venv and install Python dependencies

    python3 -m venv .venv
    source .venv/bin/activate
    pip install wheel
    pip install -r requirements.txt

### Configure

    cp config.py.example instance/config.py

### Run migrations

    PYTHONPATH=. alembic upgrade head

### Run

    python3 app.py

## Development

### Run with autoreload

    hupper -m app

### Generate migration

```shell
PYTHONPATH=. alembic revision --autogenerate
```

## Links

https://github.com/seanpar203/sanic-starter
