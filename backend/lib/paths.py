"""Various helpers to work with file paths."""


def path_from_hash_and_ext(file_hash, extension):
    """Generate path on disk for given file hash."""
    return 'uploads/{}/{}/{}.{}'.format(
        file_hash[0:2], file_hash[2:4], file_hash, extension
    )
