"""Various helpers for building queries."""
from slugify import UniqueSlugify
from sqlalchemy import exists


def instance_by_uid(session, model, id_field, uid):
    """Retrieve instance by uid."""
    return session.query(model).filter(
        getattr(model, id_field) == uid
    ).first()


def get_slug_unique_checker(session, model):
    def is_slug_unique(text, uids):
        if text in uids:
            return False
        return not session.query(exists().where(model.slug == text)).scalar()
    return is_slug_unique


def get_free_slug(session, model, name, is_to_lower):
    """Find non-conflicting slug for model instance."""
    is_slug_unique = get_slug_unique_checker(session, model)
    slugify = UniqueSlugify(to_lower=is_to_lower, unique_check=is_slug_unique)
    return slugify(name)
