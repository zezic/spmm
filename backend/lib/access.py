from functools import wraps
from sanic.response import json


def authorized(methods=['PATCH', 'PUT', 'POST', 'DELETE']):
    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            if request.method not in methods:
                response = await f(request, *args, **kwargs)
                return response
            if request.token:
                token = request.token
                user_id = await request.app.redis.get('TOKEN:{}'.format(token))
                if user_id:
                    await request.app.redis.setex(
                        'TOKEN:{}'.format(token), 60 * 60, user_id
                    )
                    request.update({
                        'user_id': user_id
                    })

                    response = await f(request, *args, **kwargs)
                    return response
            return json({}, 403)
        return decorated_function
    return decorator
