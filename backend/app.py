from sanic import Sanic
from sanic_redis import SanicRedis
from api import api
from instance import config as ic

app = Sanic(__name__)
app.config.update({
    'REDIS': {
        'address': ('127.0.0.1', 6379)
    }
})

redis = SanicRedis(app)

app.blueprint(api, url_prefix='api/')

app.run(
    debug=ic.DEBUG,
    access_log=ic.ACCESS_LOG,
    workers=ic.WORKERS
)
