"""File model."""
import datetime
from sqlalchemy import (
    Column, String, Integer, text, DateTime
)
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID

from lib.paths import path_from_hash_and_ext
from models import Base


class File(Base):
    """File model."""

    __tablename__ = 'file'

    id = Column(UUID,
                server_default=text('gen_random_uuid()'),
                primary_key=True)
    name = Column(String(256))
    ext = Column(String(10))
    size = Column(Integer)  # bytes
    md5 = Column(String(32))
    created_at = Column(DateTime, default=datetime.datetime.now())
    articles = relationship('Article')
    events = relationship('Event')
    clubs = relationship('Club')

    @property
    def fullname(self):
        """Get full file name (with extension)."""
        return '{}.{}'.format(self.name, self.ext)

    @property
    def url(self):
        """Get file URL relative to site root."""
        return '/{}'.format(path_from_hash_and_ext(self.md5, self.ext))

    def __repr__(self):
        """Represent file with it name."""
        return '<File: {}>'.format(self.fullname)
