"""User model."""
from sqlalchemy import (
    Column, String, Integer, text, DateTime
)
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID

from lib.paths import path_from_hash_and_ext
from models import Base


class User(Base):
    """User model."""

    __tablename__ = 'user'

    id = Column(UUID,
                server_default=text('gen_random_uuid()'),
                primary_key=True)
    login = Column(String(256))
    password = Column(String(128))

    def __repr__(self):
        """Represent user with it login."""
        return '<User: {}>'.format(self.login)
