"""Event model."""
import datetime
from sqlalchemy import (
    Column, String, text, DateTime, Text, Boolean, ForeignKey, Table
)
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID, ENUM

from models import Base


class Event(Base):
    """Event model."""

    __tablename__ = 'event'

    id = Column(UUID,
                server_default=text('gen_random_uuid()'),
                primary_key=True)
    name = Column(String(256))
    datetime = Column(DateTime)
    description = Column(String(10000))
    slug = Column(String(256))
    is_displayed = Column(Boolean)
    place = Column(String(256))
    picture_id = Column(UUID, ForeignKey('file.id'))
    picture = relationship('File',
                           back_populates='events',
                           foreign_keys=[picture_id])
    club_id = Column(UUID, ForeignKey('club.id'))

    def __repr__(self):
        """Represent event with it name."""
        return '<Event: {}>'.format(self.name)
