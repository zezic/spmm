"""Article model."""
import datetime
from sqlalchemy import (
    Column, String, text, DateTime, Text, Boolean, ForeignKey, Table
)
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID, ENUM

from models import Base


article_to_picture = Table(
    'article_to_picture', Base.metadata,
    Column('article_id', UUID, ForeignKey('article.id')),
    Column('file_id', UUID, ForeignKey('file.id'))
)

article_types = ('news', 'page')


class Article(Base):
    """Article model."""

    __tablename__ = 'article'

    id = Column(UUID,
                server_default=text('gen_random_uuid()'),
                primary_key=True)
    name = Column(String(256))
    body = Column(Text)
    slug = Column(String(256))
    is_displayed = Column(Boolean)
    created_at = Column(DateTime(timezone=False), default=datetime.datetime.now)
    picture_id = Column(UUID, ForeignKey('file.id'))
    picture = relationship('File',
                           back_populates='articles',
                           foreign_keys=[picture_id])
    pictures = relationship('File',
                            secondary=article_to_picture)
    type = Column(String(256))

    def __repr__(self):
        """Represent article with it name."""
        return '<Article: {}>'.format(self.name)
