# pylama:ignore=E402,W0611
"""All models."""
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

from .article import Article
from .file import File
from .user import User
from .event import Event
from .club import Club
