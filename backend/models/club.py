"""Club model."""
import datetime
from sqlalchemy import (
    Column, String, text, ForeignKey, Table, Boolean, Text, DateTime
)
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID, ENUM

from models import Base


club_to_picture = Table(
    'club_to_picture', Base.metadata,
    Column('club_id', UUID, ForeignKey('club.id')),
    Column('file_id', UUID, ForeignKey('file.id'))
)


class Club(Base):
    """Club model."""

    __tablename__ = 'club'

    id = Column(UUID,
                server_default=text('gen_random_uuid()'),
                primary_key=True)
    name = Column(String(256))
    vk_link = Column(String(256))
    leader = Column(String(256))
    is_displayed = Column(Boolean)
    body = Column(Text)
    slug = Column(String(256))
    created_at = Column(DateTime, default=datetime.datetime.now)
    picture_id = Column(UUID, ForeignKey('file.id'))
    picture = relationship('File',
                           back_populates='clubs',
                           foreign_keys=[picture_id])
    pictures = relationship('File',
                            secondary=club_to_picture)

    def __repr__(self):
        """Represent Club with it name."""
        return '<Club: {}>'.format(self.name)
