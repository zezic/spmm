# SPMM Website

## Deploying & Development

See [Backend README](backend/README.md) and [Frontend README](frontend/README.md)

## Backups

### Creating backup

Database:
```shell
pg_dump -U spmm > ../backup.sql
```

Uploads:
```shell
tar cvf ../backup.tar backend/uploads
```

### Unpacking backup

Database:
```shell
dropdb --user spmm spmm
sudo -u postgres createdb -O spmm spmm
psql --user spmm < ../backup.sql
```

Uploads:
```shell
tar xvf ../backup.tar
```