import { resolve } from 'path'
import webpack from 'webpack'

module.exports = {
  head: {
    title: 'Союз православной молодежи Мордовии',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Мордовская республиканская молодежная общественная организация «Союз православной молодежи Мордовии»' },
      { name:  'yandex-verification', content: 'd7c56a91032b6333'}
    ],
    link: [
      { rel: 'shortcut icon', type: 'image/x-icon', href: '/favicon.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap&subset=cyrillic' }
    ]
  },
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '56710792',
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
      }
    ]
  ],
  plugins: [
    { src: '~/plugins/quill-editor.js', mode: 'client' }
  //   { src: '~/plugins/nuxt-client-init.js', ssr: false }
  ],
  axios: {
    proxy: true,
    prefix: '/api',
    credentials: true
  },
  proxy: {
    '/api/': 'http://localhost:8000'
  },
  styleResources: {
    sass: ['@/assets/sass/_app.sass']
  },
  loading: { color: '#3B8070' },
  build: {
    // analyze: true,
    vendor: ['moment', 'quill'],
    plugins: [
      new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/)
    ],
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      config.node = { fs: 'empty' }
      config.module.rules.splice(0, 0, {
        test: /\.js$/,
        include: [resolve(__dirname, 'node_modules/vue2-google-maps')],
        loader: 'babel-loader',
      })
      config.module.rules.splice(0, 0, {
        test: /\.js$/,
        include: [resolve(__dirname, 'node_modules/vue-carousel')],
        loader: 'babel-loader',
      })
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
