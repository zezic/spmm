import removeHtmlComments from 'remove-html-comments'

const repairComments = function (html) {
  return html.replace(/&lt;!--/g, '<!--').replace(/--&gt;/g, '-->')
}

const removeEmptyBlocks = function (html) {
  return html.replace(/<p><br><\/p>/g, '').replace(/<p><\/p>/g, '')
}

function cleanHtml (html) {
  return !html ? '' : removeEmptyBlocks(removeHtmlComments(repairComments(html)).data)
}

export default cleanHtml
