import htmlToText from 'html-to-text'
import removeHtmlComments from 'remove-html-comments'

const repairComments = function (html) {
  if (!html) {
    return ''
  }
  return html.replace(/&lt;!--/g, '<!--').replace(/--&gt;/g, '-->')
}

function stripHTML (html) {
  return htmlToText.fromString(removeHtmlComments(repairComments(html)).data, {
    ignoreHref: true,
    ignoreImage: true
  })
}

export default stripHTML
