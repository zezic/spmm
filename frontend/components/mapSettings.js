import Vue from 'vue'
import {loaded} from '~/node_modules/vue2-google-maps/src/main'

var settings = new Vue({
  data: {
    markerIcon: {}
  }
})

export default settings

loaded.then(() => {
  settings.markerIcon = {
    url: require('~/assets/map.svg'),
    size: {width: 48, height: 45, f: 'px', b: 'px'}
  }
})
