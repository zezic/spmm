export const state = () => ({
  role: 'anonymous',
  token: null,
  loginVisible: false
})

export const mutations = {
  setToken (state, token) {
    localStorage.setItem('token', token)
    state.token = token
    this.$axios.setToken(token)
  },
  setRole (state, role) {
    state.role = role
  },
  showLogin (state) {
    state.loginVisible = true
  },
  hideLogin (state) {
    state.loginVisible = false
  },
  logout (state) {
    state.role = 'anonymous'
    localStorage.removeItem('token')
    this.$axios.setToken(false)
    state.token = null
  }
}

export const getters = {
  admin (state) {
    return state.role === 'admin'
  }
}

export const actions = {
  async tryLogin (context, payload) {
    try {
      const response = await this.$axios.$post('/session', payload)
      context.commit('setToken', response.token)
      context.commit('setRole', 'admin')
      return true
    } catch (e) {
      if (e.statusCode === 401) {
        return false
      }
    }
  },
  async checkTokenHealth (context) {
    let token = localStorage.getItem('token')
    if (!token) {
      return
    }
    this.$axios.setToken(token)
    try {
      await this.$axios.$get('/session')
      context.commit('setToken', token)
      context.commit('setRole', 'admin')
      return true
    } catch (e) {
      if (e.statusCode === 403) {
        context.commit('logout')
        return false
      }
    }
  }
}
