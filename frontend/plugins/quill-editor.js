import Vue from 'vue'
import { VueEditor, Quill } from 'vue2-editor'
window.Quill = Quill

const ImageResize = require('quill-image-resize-module').default

Quill.register('modules/imageResize', ImageResize)

Vue.component('vue-editor', VueEditor)
